﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Implementation
{
    public class RoboCop : IRobot, IPolicjant
    {
        public string Polecenie()
        {
            return "Wykonaj polecenie";
        }

        string IRobot.Polecenie()
        {
            return "P1";
        }

        string IMundurowy.Polecenie()
        {
            return "P2";
        }

        string IPolicjant.WezLapowke()
        {
            return "Bierze kase";
        }

        public string Polecenie2()
        {
            return "P";
        }
    }
}
