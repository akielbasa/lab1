﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Implementation
{
    public class Strazak : IStrazak
    {
        public string GasPozar()
        {
            return "Gasi Pożar";
        }

        public string Polecenie()
        {
            return "Wykonaj polecenie";
        }

    }
}
