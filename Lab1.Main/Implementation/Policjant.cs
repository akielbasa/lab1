﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Implementation
{
    public class Policjant : IPolicjant
    {
        
        public string WezLapowke()
        {
            return "Bierze kase";
        }

        public string Polecenie()
        {
            return "Wykonuje polecenie";
        }
    }
}
