﻿using System;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        public ICollection<IMundurowy> Metoda()
        {
            List<IMundurowy> lista = new List<IMundurowy> { };
            lista.Add(new Strazak());
            lista.Add(new Policjant());
            lista.Add(new RoboCop());

            return lista;
        }

        public void Podlicz(ICollection<IMundurowy> lista)
        {
            foreach (var test in lista)
            {
                Console.WriteLine(test.Polecenie());
            }
        }


        static void Main(string[] args)
        {
            IMundurowy m = new RoboCop();
            Console.WriteLine(m.Polecenie());
            IPolicjant p = new RoboCop();
            Console.WriteLine(p.Polecenie());
            IRobot rob = new RoboCop();
            Console.WriteLine(rob.Polecenie());

            Console.ReadKey();

        }
    }
}
